import math

#Erstellen der Schleife
wiederhole = True
while(wiederhole):
    #Eingabe der Basis-Netzadresse und Anzahl der Subnetze
    basis_netz = input("Basisnetz eingeben: ")
    arr_basis_netz = basis_netz.split('.')
    anz_subnetz = int(input("Gewünschte Anzahl der Subnetze eingeben: "))
    log_anz_subnetz = math.log2(anz_subnetz)

    #Berechnung der Anzahl der Bits für die Subnetzadressierung
    if log_anz_subnetz % 1 != 0:
        log_anz_subnetz = int(log_anz_subnetz) + 1
    else:
        log_anz_subnetz = int(log_anz_subnetz)

    host_anz_subnetz = 2**(8 - log_anz_subnetz)

    print("Anzahl der möglichen Hosts pro Netz: " + str(host_anz_subnetz - 2))

    subnet_mask = [255] * 4

    #Berechnung der Subnetzmaske
    for i in range(4):
        if log_anz_subnetz >= 8:
            subnet_mask[i] = 0
            log_anz_subnetz -= 8
        elif log_anz_subnetz > 0:
            subnet_mask[i] = 256 - 2 ** (8 - log_anz_subnetz)
            log_anz_subnetz = 0

    subnet_mask.reverse()
    print("Subnetzmaske der neuen Netze: " + ".".join(map(str,subnet_mask)))

    #Ausgabe der Subnetzadressen
    for i in range(anz_subnetz):
        print(f"Netz {i + 1}: {arr_basis_netz[0]}.{arr_basis_netz[1]}.{arr_basis_netz[2]}.{i * host_anz_subnetz}")

    # Berechnung der 8er Präfix-Subnetzadressen
    for i in range(0, anz_subnetz, 8):
        for j in range(i, min(i + 8, anz_subnetz)):
            print(f"8er Präfix-Netz {j + 1}: {arr_basis_netz[0]}.{arr_basis_netz[1]}.{arr_basis_netz[2]}.{j * host_anz_subnetz}")

    # Berechnung der 16er Präfix-Subnetzadressen
    for i in range(0, anz_subnetz, 16):
        for j in range(i, min(i + 16, anz_subnetz)):
            print(f"16er Präfix-Netz {j + 1}: {arr_basis_netz[0]}.{arr_basis_netz[1]}.{arr_basis_netz[2]}.{j * host_anz_subnetz}")

    #Beenden der Schleife
    if input("Erneute Berechnung? (Ja/Nein)") == "Nein":
        wiederhole = False
